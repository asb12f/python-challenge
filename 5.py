import urllib, pickle

myurl="http://www.pythonchallenge.com/pc/def/banner.p"

#read the url and de-serialise it

handle= urllib.urlopen(myurl)
object = pickle.load(handle)
handle.close()

#each item refers to a line in the display
for item in object:
	print "".join(i[0] * i[1] for i in item) #print characters