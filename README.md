# Project Description

This repository contains my scripted solutions for the [Python Challenge](http://www.pythonchallenge.com/).

## Progress
[Challenge 0](http://www.pythonchallenge.com/pc/def/0.html)  
[Challenge 1](http://www.pythonchallenge.com/pc/def/map.html)  
[Challenge 2](http://www.pythonchallenge.com/pc/def/ocr.html)  
[Challenge 3](http://www.pythonchallenge.com/pc/def/equality.html)  
[Challenge 4](http://www.pythonchallenge.com/pc/def/linkedlist.php)  
[Challenge 5](http://www.pythonchallenge.com/pc/def/peak.html)  
[Challenge 6](http://www.pythonchallenge.com/pc/def/channel.html)  
[Challenge 7](http://www.pythonchallenge.com/pc/def/oxygen.html)  
